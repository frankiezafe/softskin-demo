extends Spatial

var prev_mouse = null
var ry = 0

func _ready():
	pass

func _process(delta):
	
	var mp = get_viewport().get_mouse_position()
	
	if Input.is_mouse_button_pressed( BUTTON_RIGHT ):
		if prev_mouse == null:
			prev_mouse = mp
		else:
			var diff = mp - prev_mouse
			var nry = -diff.x / get_viewport().size.x * 360 * delta
			ry += ( nry - ry ) * 0.3
#			rotate_z( diff.y* delta )
			prev_mouse = mp
	else:
		ry *= 0.95
		prev_mouse = null
		
	rotate_y( ry )
	
	pass
