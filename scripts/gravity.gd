tool

extends Position3D

export(float, 0, 10) var strength = 0
export(float, 0, 1) var damping = 0

var front = Vector3( 0,0,1 )
var smooth_g = Quat()

func _ready():
	pass # Replace with function body.

func _process(delta):
	
	var q = Quat()
	q.set_euler( transform.basis.get_euler() )
	smooth_g = smooth_g.slerp( q, damping )
	var t = Transform( smooth_g )
	
	var tmp_g = t.xform( front ) * strength
	
	var skin = get_node("../Skin")
	q.set_euler( skin.transform.basis.get_euler() )
	t = Transform( q.inverse() )
	skin.set_gravity( t.xform( tmp_g ) )
	
	pass
