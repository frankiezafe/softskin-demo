tool

extends ImmediateGeometry

func _ready():
	
	var base0 = Vector3(-0.05, 0, -0.05)
	var base1 = Vector3(0.05, 0, -0.05)
	var base2 = Vector3(0.05, 0, 0.05)
	var base3 = Vector3(-0.05, 0, 0.05)
	var top = Vector3(0, 0.8, 0)
	
	begin(Mesh.PRIMITIVE_TRIANGLES)
	add_vertex(base1)
	add_vertex(base0)
	add_vertex(top)
	add_vertex(base0)
	add_vertex(base1)
	add_vertex(top)
	
	add_vertex(base2)
	add_vertex(base1)
	add_vertex(top)
	add_vertex(base1)
	add_vertex(base2)
	add_vertex(top)
	
	add_vertex(base3)
	add_vertex(base2)
	add_vertex(top)
	add_vertex(base2)
	add_vertex(base3)
	add_vertex(top)
	
	add_vertex(base0)
	add_vertex(base3)
	add_vertex(top)
	add_vertex(base3)
	add_vertex(base0)
	add_vertex(top)
	
	end()
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
