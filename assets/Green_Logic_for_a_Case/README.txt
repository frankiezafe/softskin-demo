                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2334520
Green Logic for a Case by shivinteger is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

**FAQ**: http://www.thingiverse.com/shivinteger/about 

[>>>] Generating Attributions log: 
 
[0] Thing Title : Dice Pack
[0] Thing URL : http://www.thingiverse.com/thing:372
[0] Thing Author : MaskedRetriever
[0] Thing Licence : Public Domain

[1] Thing Title : 40mm Cube Test Object
[1] Thing URL : http://www.thingiverse.com/thing:477
[1] Thing Author : bre
[1] Thing Licence : Public Domain

[2] Thing Title : Trautman Hook (OpenProsthetics)
[2] Thing URL : http://www.thingiverse.com/thing:2194
[2] Thing Author : Erik
[2] Thing Licence : Public Domain

[3] Thing Title : Mini-Maker
[3] Thing URL : http://www.thingiverse.com/thing:2626
[3] Thing Author : tc_fea
[3] Thing Licence : Public Domain

[4] Thing Title : 3D Printer Accessory Holder
[4] Thing URL : http://www.thingiverse.com/thing:3505
[4] Thing Author : ElectronicKit
[4] Thing Licence : Public Domain

[5] Thing Title : Green Lantern Ring
[5] Thing URL : http://www.thingiverse.com/thing:3544
[5] Thing Author : itchyd
[5] Thing Licence : Public Domain

[6] Thing Title : Plastic Welding Gun (Plastruder MK4)
[6] Thing URL : http://www.thingiverse.com/thing:4156
[6] Thing Author : donutman_2000
[6] Thing Licence : Public Domain

[7] Thing Title : Laser mount
[7] Thing URL : http://www.thingiverse.com/thing:5606
[7] Thing Author : PieterBos
[7] Thing Licence : Public Domain

[8] Thing Title : Open Bench Logic Sniffer Case
[8] Thing URL : http://www.thingiverse.com/thing:6877
[8] Thing Author : Madox
[8] Thing Licence : Public Domain

[9] Thing Title : Propeller
[9] Thing URL : http://www.thingiverse.com/thing:7779
[9] Thing Author : Brinny
[9] Thing Licence : Public Domain

[10] Thing Title : RC Airplane Bomb Drop
[10] Thing URL : http://www.thingiverse.com/thing:14190
[10] Thing Author : foxdewayne
[10] Thing Licence : Public Domain

[+] Attributions logged 


[+] Getting object #0
[<] Importing : downloads/14190/Nose_end.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.35%, F: -0.52%
[+] Preparing : Nose end

[+] Getting object #1
[<] Importing : downloads/5573/0.5mm-thin-wall.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : 0.5Mm-Thin-Wall
[+] Rotating object = 0.5Mm-Thin-Wall
[!] Volume difference is 82.57%
[+] Created duplicate : Nose end.001
[+] Staging objects : ON_TOP
[+] Union between : Nose end.001 & 0.5Mm-Thin-Wall
[!] Number of faces #1 : 571 + number of faces #2 : 320 = 891
[!] Number of faces of union: 943
[+] Cleaning non manifold
[!] Total removed V: -23.66%, E: -22.19%, F: -20.78%
[-] Object is malformed
[-] Union did not work well. Discarding object.

[+] Getting object #2
[<] Importing : downloads/10065/7.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : 7
[+] Rotating object = 7
[!] Volume difference is 2310.53%
[+] Scaled 7 by 75.64%
[+] Created duplicate : Nose end.001
[+] Staging objects : BOTH_BACK
[+] Union between : Nose end.001 & 7
[!] Number of faces #1 : 571 + number of faces #2 : 1920 = 2491
[!] Number of faces of union: 2809
[+] Cleaning non manifold
[!] Total removed V: -93.61%, E: -94.65%, F: -94.59%
[-] Object is malformed
[-] Union did not work well. Discarding object.

[+] Getting object #3
[<] Importing : downloads/477/40mmcube.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : 40Mmcube
[+] Rotating object = 40Mmcube
[!] Volume difference is 1925.05%
[+] Scaled 40Mmcube by 80.39%
[+] Created duplicate : Nose end.001
[+] Staging objects : BOTH_RIGHT
[+] Union between : Nose end.001 & 40Mmcube
[!] Number of faces #1 : 571 + number of faces #2 : 12 = 583
[!] Number of faces of union: 617
[+] Cleaning non manifold
[!] Total removed V: -63.55%, E: -64.23%, F: -64.18%
[-] Object is malformed
[-] Union did not work well. Discarding object.

[+] Getting object #4
[<] Importing : downloads/4156/trigger.STL
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : trigger
[+] Rotating object = trigger
[!] Volume difference is 216.29%
[+] Created duplicate : Nose end.001
[+] Staging objects : BOTH_LEFT
[+] Union between : Nose end.001 & trigger
[!] Number of faces #1 : 571 + number of faces #2 : 80 = 651
[!] Number of faces of union: 677
[+] Cleaning non manifold
[!] Total removed V: -0.24%, E: -0.18%, F: 0.00%

[+] Getting object #5
[<] Importing : downloads/16122/Sopa_Coin.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.44%, F: -0.66%
[+] Preparing : Sopa Coin
[+] Rotating object = Sopa Coin
[!] Volume difference is 681.05%
[+] Created duplicate : Nose end.000
[+] Staging objects : BOTH_TOP
[+] Union between : Nose end.000 & Sopa Coin
[!] Number of faces #1 : 677 + number of faces #2 : 1053 = 1730
[!] Number of faces of union: 1912
[+] Cleaning non manifold
[!] Total removed V: -73.12%, E: -70.60%, F: -67.63%
[-] Object is malformed
[-] Union did not work well. Discarding object.

[+] Getting object #6
[<] Importing : downloads/5756/fan.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.05%, F: -0.07%
[+] Preparing : Fan
[+] Rotating object = Fan
[!] Volume difference is 358.20%
[+] Created duplicate : Nose end.000
[+] Staging objects : BOTH_BACK
[+] Union between : Nose end.000 & Fan
[!] Number of faces #1 : 677 + number of faces #2 : 1345 = 2022
[!] Number of faces of union: 2308
[+] Cleaning non manifold
[!] Total removed V: -74.75%, E: -73.82%, F: -72.88%
[-] Object is malformed
[-] Union did not work well. Discarding object.

[+] Getting object #7
[<] Importing : downloads/15362/newdalek.stl
[+] Cleaning non manifold
[!] Total removed V: -1.48%, E: -2.40%, F: -2.83%
[-] Object is malformed
[+] Deleting bad source : downloads/15362/newdalek.stl

[+] Getting object #7
[<] Importing : downloads/2626/mini-maker_100427a_plastruder.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Mini-Maker 100427A Plastruder
[+] Rotating object = Mini-Maker 100427A Plastruder
[!] Volume difference is 391.27%
[+] Created duplicate : Nose end.000
[+] Staging objects : ON_TOP
[+] Union between : Nose end.000 & Mini-Maker 100427A Plastruder
[!] Number of faces #1 : 677 + number of faces #2 : 508 = 1185
[!] Number of faces of union: 1187
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #8
[<] Importing : downloads/3505/sepgonftpen.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Sepgonftpen
[+] Rotating object = Sepgonftpen
[!] Volume difference is 31.04%
[+] Created duplicate : Nose end.001
[+] Staging objects : ON_FRONT
[+] Union between : Nose end.001 & Sepgonftpen
[!] Number of faces #1 : 1187 + number of faces #2 : 80 = 1267
[!] Number of faces of union: 1271
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #9
[<] Importing : downloads/477/40mmcube.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : 40Mmcube
[+] Rotating object = 40Mmcube
[!] Volume difference is 10.19%
[+] Created duplicate : Nose end.000
[+] Staging objects : ON_BACK
[+] Union between : Nose end.000 & 40Mmcube
[!] Number of faces #1 : 1271 + number of faces #2 : 12 = 1283
[!] Number of faces of union: 1289
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #10
[<] Importing : downloads/5606/LaserholderCapMeccano.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.55%, F: -0.83%
[+] Preparing : LaserholderCapMeccano
[+] Rotating object = LaserholderCapMeccano
[!] Volume difference is 11.21%
[+] Created duplicate : Nose end.001
[+] Staging objects : ON_TOP
[+] Union between : Nose end.001 & LaserholderCapMeccano
[!] Number of faces #1 : 1289 + number of faces #2 : 956 = 2245
[!] Number of faces of union: 2288
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #11
[<] Importing : downloads/3544/green_lantern_ring.STL
[+] Cleaning non manifold
[!] Total removed V: -0.28%, E: -0.38%, F: -0.37%
[+] Preparing : green lantern ring
[+] Rotating object = green lantern ring
[!] Volume difference is 0.54%
[+] Scaled green lantern ring by 264.98%
[+] Created duplicate : Nose end.000
[+] Staging objects : BOTH_TOP
[+] Union between : Nose end.000 & green lantern ring
[!] Number of faces #1 : 2288 + number of faces #2 : 15460 = 17748
[!] Number of faces of union: 17348
[+] Cleaning non manifold
[!] Total removed V: -0.01%, E: -0.00%, F: 0.00%

[+] Getting object #12
[<] Importing : downloads/2194/trautman-base.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.00%, F: -0.00%
[+] Preparing : Trautman-Base
[+] Rotating object = Trautman-Base
[!] Volume difference is 0.00%
[+] Scaled Trautman-Base by 5252.21%
[+] Created duplicate : Nose end.001
[+] Staging objects : ON_BACK
[+] Union between : Nose end.001 & Trautman-Base
[!] Number of faces #1 : 17348 + number of faces #2 : 39983 = 57331
[!] Number of faces of union: 56351
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #13
[<] Importing : downloads/477/40mmcube.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : 40Mmcube
[+] Rotating object = 40Mmcube
[!] Volume difference is 6.51%
[+] Scaled 40Mmcube by 115.39%
[+] Created duplicate : Nose end.000
[+] Staging objects : BOTH_LEFT
[+] Union between : Nose end.000 & 40Mmcube
[!] Number of faces #1 : 56351 + number of faces #2 : 12 = 56363
[!] Number of faces of union: 55860
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #14
[<] Importing : downloads/6877/OpenLogicCase.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : OpenLogicCase
[+] Rotating object = OpenLogicCase
[!] Volume difference is 1.63%
[+] Scaled OpenLogicCase by 182.90%
[+] Created duplicate : Nose end.001
[+] Staging objects : ON_RIGHT
[+] Union between : Nose end.001 & OpenLogicCase
[!] Number of faces #1 : 55860 + number of faces #2 : 15342 = 71202
[!] Number of faces of union: 69428
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #15
[<] Importing : downloads/7779/base_plate.STL
[+] Cleaning non manifold
[!] Total removed V: -0.32%, E: -0.28%, F: -0.21%
[+] Preparing : base plate
[+] Rotating object = base plate
[!] Volume difference is 0.26%
[+] Scaled base plate by 338.10%
[+] Created duplicate : Nose end.000
[+] Staging objects : ON_BOTTOM
[+] Union between : Nose end.000 & base plate
[!] Number of faces #1 : 69428 + number of faces #2 : 1898 = 71326
[!] Number of faces of union: 71344
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[+] Getting object #16
[<] Importing : downloads/9710/toy-truck10-v2a.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: -0.79%, F: -1.19%
[-] Object is malformed
[+] Deleting bad source : downloads/9710/toy-truck10-v2a.stl

[+] Getting object #16
[<] Importing : downloads/372/die_scaled_d10.stl
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%
[+] Preparing : Die Scaled D10
[+] Rotating object = Die Scaled D10
[!] Volume difference is 0.01%
[+] Scaled Die Scaled D10 by 1120.89%
[+] Created duplicate : Nose end.001
[+] Staging objects : DEFAULT
[+] Union between : Nose end.001 & Die Scaled D10
[!] Number of faces #1 : 71344 + number of faces #2 : 20 = 71364
[!] Number of faces of union: 67817
[+] Cleaning non manifold
[!] Total removed V: 0.00%, E: 0.00%, F: 0.00%

[>] Exporting STL to : /uploads/20170521-1040-23