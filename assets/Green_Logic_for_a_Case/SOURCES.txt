Sources for Green Logic for a Case

40mm Cube Test Object (https://www.thingiverse.com/thing:477)
Mini-Maker (https://www.thingiverse.com/thing:2626)
3D Printer Accessory Holder (https://www.thingiverse.com/thing:3505)
Green Lantern Ring (https://www.thingiverse.com/thing:3544)
Plastic Welding Gun (Plastruder MK4) (https://www.thingiverse.com/thing:4156)
Open Bench Logic Sniffer Case (https://www.thingiverse.com/thing:6877)
Propeller (https://www.thingiverse.com/thing:7779)
