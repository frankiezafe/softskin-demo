#!/usr/bin/python
import os,re

files = [os.path.join(dp, f) for dp, dn, filenames in os.walk('.') for f in filenames if os.path.splitext(f)[1] == '.tscn']

for f in files:
	try:
		new_content = ''
		need_rewrite = False
		file = open(f,'r')
		for line in file:
			if line.find( 'type="Skin' ) != -1:
				need_rewrite = True
				new_content += line.replace( 'type="Skin', 'type="Softskin' )
			else:
				new_content += line
		file.close()
		if need_rewrite:
			file = open(f,'w')
			file.write( new_content )
			file.close()
	except:
		print( "failed to process: " + f )
#print( files )